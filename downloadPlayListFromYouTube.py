import sys
import threading
from pytube import YouTube
from pytube import Playlist
from math import ceil

try:
    p = Playlist(input("Введите ссылку на Playlist: "))
except:
    pass
# global links
print("Playlist Name : {}\nChannel Name  : {}\nTotal Videos  : {}\nTotal Views   : {}".format(
    p.title, p.owner, p.length, p.views))
links = []
size = 0
try:
    for url in p.video_urls:
        links.append(url)
except:
    print('Playlist не доступен!')
    sys.exit(0)
size = ceil(len(links)/4)


def split_link(links, size):
    for i in range(0, len(links), size):
        yield links[i:i+size]


link = list(split_link(links, size))

print("Загрузка...\n")


def Thread_1():
    for i in link[0]:
        yt = YouTube(i)
        ys = yt.streams.get_highest_resolution()
        filename = ys.download()
        print("Поток 1 -->  " + filename.split('/')[-1] + ' Загружено!')


def Thread_2():
    for i in link[1]:
        yt = YouTube(i)
        ys = yt.streams.get_highest_resolution()
        filename = ys.download()
        print("Поток 2 -->  " + filename.split('/')[-1] + ' Загружено!')


def Thread_3():
    for i in link[2]:
        yt = YouTube(i)
        ys = yt.streams.get_highest_resolution()
        filename = ys.download()
        print("Поток 3 -->  " + filename.split('/')[-1] + ' Загружено!')


def Thread_4():
    for i in link[3]:
        yt = YouTube(i)
        ys = yt.streams.get_highest_resolution()
        filename = ys.download()
        print("Поток 4 -->  " + filename.split('/')[-1] + ' Загружено!')


t1 = threading.Thread(target=Thread_1, name='d_1')
t2 = threading.Thread(target=Thread_2, name='d_2')
t3 = threading.Thread(target=Thread_3, name='d_3')
t4 = threading.Thread(target=Thread_4, name='d_4')

try:
    t1.start()
    t2.start()
    t3.start()
    t4.start()
except IndexError as e:
    pass

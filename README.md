# Загрузка PlayList из YouTube

## Установка и настройка

- Python v.3.9+
- Установите необходимые пакеты для работы `python -m pip install -r requirements.txt`


## Пример 

![image_1](./img/1.png)<br>
![image_2](./img/2.png)<br>

- Берем интересующий нас канал, например этот PlayList: <br>`https://www.youtube.com/playlist?list=PLtPJ9lKvJ4oh5SdmGVusIVDPcELrJ2bsT`
<br>НЕ РЕКЛАМА! :smirk:

- Вставляем ссылку PlayList в терминал, нажимаем `Ввод` и ждем :innocent:
## Ошибки в работе

- Необходимо поменять регулярное выражение в классе Cipher:

```py
class Cipher:
    def __init__(self, js: str):
        self.transform_plan: List[str] = get_transform_plan(js)
        # Данное выражение
        var_regex = re.compile(r"^\w+\W")
        # Меняем на это
        var_regex = re.compile(r"^\$*\w+\W")
```        
